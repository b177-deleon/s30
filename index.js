const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;

mongoose.connect("mongodb+srv://admin:PVTvwGg1NxoEsIY9@wdc028-course-booking.rd7vc.mongodb.net/b177-to-do?retryWrites=true&w=majority",{
    useNewUrlParser: true,
    useUnifiedTopology: true
})

// Set notification for connection success or failure
// Connection to the database
let db = mongoose.connection;

// If a connection error occured, output a message in the console
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, output a message in the console
db.once("open", () => console.log("We're connected to the cloud database"))

// Create a Task schema
const taskSchema = new mongoose.Schema({
    name: String,
    status: {
        type: String,
        // Default values are predefined values for a field
        default: "pending"
    }
})

// ACTIVITY - User Schema
const userSchema = new mongoose.Schema({
    username: String,
    password: String,
})

// Create Models
// Server > Schema > Database > Collection
const Task = mongoose.model("Task", taskSchema);

// ACTIVITY - User Model
const User =  mongoose.model("User", userSchema);

app.use(express.json());

app.use(express.urlencoded({extended:true}));

// Create a POST route to create a new task
app.post("/tasks", (req, res) => {
    Task.findOne({name: req.body.name}, (err, result) => {
        // If a document was found and document's name matches the information sent via the client/postman
        if(result != null && result.name == req.body.name){
            // Returns a message to the client/postman
            return res.send("Duplicate task found")
        }
        // If no document found
        else{
            // Create a new task and save it to the database
            let newTask = new Task({
                name : req.body.name
            })

            newTask.save((saveErr, savedTask) => {
                // If there are errors in saving
                if(saveErr){
                    return console.error(saveErr);
                }
                // if no error found while creating the document
                else{
                    return res.status(201).send("New task created")
                }
            })
        }
    })
})

// ACTIVITY - POST User request
app.post("/signup", (req, res) => {
    User.findOne({username: req.body.username}, (err, result) => {
        if(result != null && result.username == req.body.username){
            return res.send("User already exists!")
        }
        if(req.body.username !== '' && req.body.password !== ''){
            let newUser = new User({
                username : req.body.username,
                password : req.body.password
            })

            newUser.save((saveErr, savedUser) => {
                if(saveErr){
                    return console.error(saveErr);
                }
                else{
                    return res.send(`User ${req.body.username} successfully registered!`)
                }
            })
        }
        else{
            res.send("Please input BOTH username and password.")
        }
    })

})

// ACTIVITY - GET all users
app.get("/users", (req, res) => {
    User.find({}, (err, result) => {
        if(err){
            return console.log(err)
        }
        else{
            return res.status(200).json({
                data : result
            })
        }
    })
})

// Create a GET request to retrieve all tasks
app.get("/tasks", (req, res) => {
    Task.find({}, (err, result) => {
        // If an error occured
        if(err){
            // will print any error in the console
            return console.log(err)
        }
        else{
            return res.status(200).json({
                data : result
            })
        }
    })
})

app.listen(port, () => console.log(`Server running at port ${port}`));